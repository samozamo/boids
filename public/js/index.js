"use strict";
import { generateOptions, options } from "./options.js";
import { makeBirdArmy, updateBirds, paintCanvas } from "./utils.js";
import { state } from "./state.js";
import Timer from "./Timer.js";
import { setupEventHandlers } from "./events.js";

const setup = (state) => {
  state.entities = makeBirdArmy(64);
  generateOptions(options(state));
  setupEventHandlers(state);
};

const resizeCanvas = (canvas) => {
  const DEFAULT_LENGTH = 400;
  const viewportWidth = window.innerWidth;
  const viewportHeight = window.innerHeight;

  state.canvas.height =
    viewportHeight < DEFAULT_LENGTH ? viewportHeight : DEFAULT_LENGTH;
  state.canvas.width =
    viewportWidth < DEFAULT_LENGTH ? viewportWidth : DEFAULT_LENGTH;

  canvas.setAttribute("height", state.canvas.height);
  canvas.setAttribute("width", state.canvas.width);
};

const createStep = () => {
  const canvas = document.querySelector("canvas");
  const ctx = canvas.getContext("2d");
  resizeCanvas(canvas);

  window.addEventListener("resize", () => {
    resizeCanvas(canvas);
  });

  const step = () => {
    updateBirds(state);
    paintCanvas(ctx, state);
  };

  return step;
};

const main = () => {
  setup(state);
  const refreshRate = 1 / 144;
  const timer = new Timer(refreshRate);
  timer.update = createStep();
  timer.start();

  document.addEventListener("visibilitychange", (e) => {
    console.log(document.visibilityState);
    if (document.visibilityState === "visible") {
      timer.start();
    }

    if (document.visibilityState === "hidden") {
      timer.stop();
    }
  });
};

main();
