import { Vec2 } from "./Vec2.js";

export default class Bird {
  constructor(options) {
    this.velocity = new Vec2(options.vector.x, options.vector.y);
    this.pos = new Vec2(options.pos.x, options.pos.y);
    this.accel = new Vec2(0, 0);
    this.maxSpeedModifier = options.maxSpeedModifier;
  }
}
