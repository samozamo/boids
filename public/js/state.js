import { Vec2 } from "./Vec2.js";

export const state = {
  canvas: {
    height: 400,
    width: 400,
  },
  entities: [],
  avoidTargets: [],
  mouse: {
    pos: null,
    direction: new Vec2(0, 0),
  },
  separation: {
    multiplier: 0.6,
    radius: 30,
  },
  cohesion: {
    multiplier: 0.2,
    radius: 120,
  },
  alignment: {
    multiplier: 0.4,
    radius: 120,
  },
  obstacle: {
    multiplier: 15,
    radius: 120,
    angle: (2 * Math.PI) / 6,
  },
  maxSpeed: 1.5,
  colours: ["#5DFFD3", "#5DFFFC", "#5DDAFF", "#5DB2FF", "#5D89FF"],
};
