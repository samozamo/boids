import { state } from "./state.js";
import { Vec2 } from "./Vec2.js";
import Bird from "./Bird.js";

export const realMod = (i, base) => {
  const j = i % base;
  return j < 0 ? j + base : j;
};

export const birdOptions = (
  pos = new Vec2(
    Math.random() * state.canvas.width,
    Math.random() * state.canvas.height
  )
) => {
  const vectorX = 2 * Math.random() - 1;
  const vectorY = 2 * Math.random() - 1;
  const maxSpeedModifier = 0.4 * Math.random() + 0.8;
  return {
    vector: new Vec2(vectorX, vectorY).setMag(state.maxSpeed),
    pos: new Vec2(pos.x, pos.y),
    maxSpeedModifier,
  };
};

export const makeBirdArmy = (size) => {
  const birdArmy = [];
  for (let i = 0; i < size; i++) {
    const options = birdOptions();
    const bird = new Bird(options);
    birdArmy.push(bird);
  }
  return birdArmy;
};

export const boidForces = (current, state) => {
  const separationForce = new Vec2(0, 0);
  const alignmentForce = new Vec2(0, 0);
  const cohesionForce = new Vec2(0, 0);
  let count = 0;

  for (let i = 0; i < state.entities.length; i++) {
    if (current === state.entities[i]) {
      continue;
    }
    const distance = Vec2.distance(
      current.pos,
      state.entities[i].pos,
      state.canvas.width,
      state.canvas.height
    );
    const vector = Vec2.get(
      state.entities[i].pos,
      current.pos,
      state.canvas.width,
      state.canvas.height
    );
    if (distance <= state.cohesion.radius) {
      cohesionForce.add(vector);
      count++;
    }
    if (distance <= state.alignment.radius) {
      alignmentForce.add(
        state.entities[i].velocity.clone().normalize().divide(distance)
      );
    }
    if (distance <= state.separation.radius) {
      separationForce.add(
        vector
          .clone()
          .normalize()
          .multiply(-1 / distance)
      );
    }
  }
  if (count > 0) {
    cohesionForce.divide(count).setMag(0.05);
  }

  return {
    separationForce,
    alignmentForce,
    cohesionForce,
  };
};

export const obstacleForces = (current, state) => {
  const sum = new Vec2(0, 0);
  if (state.mouse.pos) {
    const dis = Vec2.distance(
      state.mouse.pos,
      current.pos,
      state.canvas.width,
      state.canvas.height
    );
    if (dis < state.obstacle.radius) {
      const vector = Vec2.get(
        state.mouse.pos,
        current.pos,
        state.canvas.width,
        state.canvas.height
      );
      if (Vec2.angle(current.velocity, vector) <= state.obstacle.angle) {
        vector.normalize().divide(dis);
        sum.add(vector);
      }
    }
  }

  for (let i = 0; i < state.avoidTargets.length; i++) {
    const dis = Vec2.distance(
      state.avoidTargets[i],
      current.pos,
      state.canvas.width,
      state.canvas.height
    );
    if (dis < state.obstacle.radius) {
      const vector = Vec2.get(
        state.avoidTargets[i],
        current.pos,
        state.canvas.width,
        state.canvas.height
      );
      if (Vec2.angle(current.velocity, vector) <= state.obstacle.angle) {
        vector.normalize().divide(dis);
        sum.add(vector);
      }
    }
  }
  sum.truncate(state.maxSpeed);
  return sum;
};

export const updateBirds = (state) => {
  for (let i = 0; i < state.entities.length; i++) {
    // Get Forces
    const { separationForce, alignmentForce, cohesionForce } = boidForces(
      state.entities[i],
      state
    );
    const obstacleForce = obstacleForces(state.entities[i], state);
    const noise = new Vec2(2 * Math.random() - 1, 2 * Math.random() - 1);

    // Adjust Forces
    separationForce.multiply(state.separation.multiplier);
    alignmentForce.multiply(state.alignment.multiplier);
    cohesionForce.multiply(state.cohesion.multiplier);
    noise.multiply(0.01);
    obstacleForce.multiply(state.obstacle.multiplier);

    // Combine Forces
    const accel = new Vec2(0, 0);
    accel
      .add(separationForce)
      .add(alignmentForce)
      .add(cohesionForce)
      .add(obstacleForce)
      .add(noise);

    // Apply acceleration to boid's velocity
    state.entities[i].accel.add(accel).truncate(state.maxSpeed);
    state.entities[i].velocity
      .add(state.entities[i].accel)
      .truncate(state.maxSpeed * state.entities[i].maxSpeedModifier);

    // Set new Position
    const newPosX = realMod(
      state.entities[i].pos.x + state.entities[i].velocity.x,
      state.canvas.width
    );
    const newPosY = realMod(
      state.entities[i].pos.y + state.entities[i].velocity.y,
      state.canvas.height
    );
    state.entities[i].pos = new Vec2(newPosX, newPosY);
  }
};

export const paintCanvas = (ctx, state) => {
  ctx.fillStyle = "rgba(0,0,0,0.1)";
  ctx.fillRect(0, 0, state.canvas.width, state.canvas.height);

  for (let i = 0; i < state.avoidTargets.length; i++) {
    ctx.beginPath();
    ctx.moveTo(state.avoidTargets[i].x, state.avoidTargets[i].y);
    ctx.arc(
      state.avoidTargets[i].x,
      state.avoidTargets[i].y,
      2,
      0,
      2 * Math.PI
    );
    ctx.fillStyle = "rgba(255,255,255,0.8)";
    ctx.fill();
  }

  for (let i = 0; i < state.entities.length; i++) {
    ctx.fillStyle = state.colours[i % state.colours.length];
    ctx.fillRect(
      state.entities[i].pos.x - 1,
      state.entities[i].pos.y - 1,
      2,
      2
    );
  }
};
