export class Vec2 {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  get length() {
    return (this.x ** 2 + this.y ** 2) ** 0.5;
  }

  normalize() {
    const length = this.length;
    if (length !== 0) {
      this.x = this.x / length;
      this.y = this.y / length;
    } else {
      this.x = 0;
      this.y = 0;
    }
    return this;
  }

  add(vector) {
    this.x += vector.x;
    this.y += vector.y;
    return this;
  }

  subtract(vector) {
    this.x -= vector.x;
    this.y -= vector.y;
    return this;
  }

  multiply(t) {
    this.x *= t;
    this.y *= t;
    return this;
  }

  divide(t) {
    if (t !== 0) {
      this.x /= t;
      this.y /= t;
    }
    return this;
  }

  truncate(t) {
    const length = this.length;
    if (length > t) {
      this.x = (this.x * t) / length;
      this.y = (this.y * t) / length;
    }
    return this;
  }

  setMag(t) {
    const length = this.length;
    this.x = (this.x * t) / length || 0;
    this.y = (this.y * t) / length || 0;
    return this;
  }

  clone() {
    return new Vec2(this.x, this.y);
  }

  static get(p1, p2, maxWidth, maxHeight) {
    let dx = p1.x - p2.x;
    let dy = p1.y - p2.y;
    const dxAbs = Math.abs(dx);
    const dyAbs = Math.abs(dy);

    if (dxAbs > maxWidth / 2) {
      const direction = dx / dxAbs;
      dx = maxWidth - dxAbs;
      dx = dx * -direction;
    }

    if (dyAbs > maxHeight / 2) {
      const direction = dy / dyAbs;
      dy = maxHeight - dyAbs;
      dy = dy * -direction;
    }

    return new Vec2(dx, dy);
  }

  static distance(p1, p2, maxWidth, maxHeight) {
    let dx = Math.abs(p1.x - p2.x);
    if (dx > maxWidth / 2) {
      dx = maxWidth - dx;
    }

    let dy = Math.abs(p1.y - p2.y);
    if (dy > maxHeight / 2) {
      dy = maxHeight - dy;
    }
    return (dx ** 2 + dy ** 2) ** 0.5;
  }

  static angle(vector1, vector2) {
    const dot = vector1.x * vector2.x + vector1.y * vector2.y;
    const magnitudeOne = (vector1.x ** 2 + vector1.y ** 2) ** 0.5;
    const magnitudeTwo = (vector2.x ** 2 + vector2.y ** 2) ** 0.5;
    const angle = Math.acos(dot / (magnitudeOne * magnitudeTwo));
    return angle; // Returns angle between 0 - Math.PI
  }
}
