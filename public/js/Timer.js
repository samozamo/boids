export default class Timer {
  constructor(deltaTime = 0) {
    this.accumTime = 0;
    this.lastTime = 0;
    this.deltaTime = deltaTime;
    this.animationId;

    this.stepProxy = (time) => {
      if (!this.lastTime) this.lastTime = time;
      this.accumTime += (time - this.lastTime) / 1000;

      while (this.accumTime > deltaTime) {
        this._update();
        this.accumTime -= deltaTime;
      }

      this.lastTime = time;

      this.animationId = this.enqueue();
    };
  }

  _update() {
    return;
  }

  get update() {
    this._update();
  }

  set update(fn) {
    this._update = fn;
  }

  enqueue() {
    return window.requestAnimationFrame(this.stepProxy);
  }

  start() {
    this.animationId = this.enqueue();
  }

  stop() {
    this.lastTime = 0;
    this.accumTime = 0;
    window.cancelAnimationFrame(this.animationId);
  }
}
