import Bird from "./Bird.js";
import { Vec2 } from "./Vec2.js";

const canvas = document.querySelector("canvas");

export const setupEventHandlers = (state) => {
  canvas.addEventListener("mousemove", (e) => {
    state.mouse.pos = {
      x: e.offsetX,
      y: e.offsetY,
    };
  });

  canvas.addEventListener("mouseleave", (e) => {
    state.mouse.pos = null;
  });

  canvas.addEventListener("click", (e) => {
    state.avoidTargets.push(new Vec2(e.offsetX, e.offsetY));
  });

  canvas.addEventListener("contextmenu", (e) => {
    e.preventDefault();
    state.avoidTargets.pop();
  });
};
