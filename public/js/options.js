import Bird from "./Bird.js";
import { birdOptions } from "./utils.js";

export const options = (state) => [
  {
    attrs: {
      type: "range",
      id: "birds",
      label: "Birds",
      min: 0,
      max: 100,
      step: 1,
      get value() {
        return state.entities.length;
      },
    },
    data: {
      defaultValue: state.entities.length,
    },
    update(e) {
      if (state.entities.length > e.target.value) {
        state.entities = [...state.entities.slice(0, e.target.value)];
      } else {
        const newBirds = [];
        for (let i = 0; i < e.target.value - state.entities.length; i++) {
          const bird = new Bird({
            ...birdOptions(),
          });
          newBirds.push(bird);
        }
        state.entities = [...state.entities, ...newBirds];
      }
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "maxSpeed",
      label: "Max Speed",
      min: 0.5,
      max: 2,
      step: 0.05,
      value: state.maxSpeed,
    },
    data: {
      defaultValue: state.maxSpeed,
    },
    update(e) {
      state.maxSpeed = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "separationMultiplier",
      label: "Separation Force",
      min: 0,
      max: 1,
      step: 0.01,
      value: state.separation.multiplier,
    },
    data: {
      defaultValue: state.separation.multiplier,
    },
    update(e) {
      state.separation.multiplier = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "cohesionMultiplier",
      label: "Cohesion Force",
      min: 0,
      max: 1,
      step: 0.01,
      value: state.cohesion.multiplier,
    },
    data: {
      defaultValue: state.cohesion.multiplier,
    },
    update(e) {
      state.cohesion.multiplier = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "alignmentMultiplier",
      label: "Alignment Force",
      min: 0,
      max: 1,
      step: 0.01,
      value: state.alignment.multiplier,
    },
    data: {
      defaultValue: state.alignment.multiplier,
    },
    update(e) {
      state.alignment.multiplier = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "targetMultiplier",
      label: "Point of Interest Force",
      min: -20,
      max: 20,
      step: 1,
      value: state.obstacle.multiplier,
    },
    data: {
      defaultValue: state.obstacle.multiplier,
    },
    update(e) {
      state.obstacle.multiplier = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "separationRadius",
      label: "Separation Radius",
      min: 0,
      max: 120,
      step: 1,
      value: state.separation.radius,
    },
    data: {
      defaultValue: state.separation.radius,
    },
    update(e) {
      state.separation.radius = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "cohesionRadius",
      label: "Cohesion Radius",
      min: 0,
      max: 120,
      step: 1,
      value: state.cohesion.radius,
    },
    data: {
      defaultValue: state.cohesion.radius,
    },
    update(e) {
      state.cohesion.radius = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "alignmentRadius",
      label: "Alignment Radius",
      min: 0,
      max: 120,
      step: 1,
      value: state.alignment.radius,
    },
    data: {
      defaultValue: state.alignment.radius,
    },
    update(e) {
      state.alignment.radius = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
  {
    attrs: {
      type: "range",
      id: "obstacleRadius",
      label: "Point of Interest Radius",
      min: 0,
      max: 240,
      step: 1,
      value: state.obstacle.radius,
    },
    data: {
      defaultValue: state.obstacle.radius,
    },
    update(e) {
      state.obstacle.radius = e.target.value;
      this.setAttribute("value", e.target.value);
    },
  },
];

export const generateOptions = (options) => {
  options.forEach((option) => {
    const input = document.createElement("input");
    for (const attr in option.attrs) {
      input.setAttribute(attr, option.attrs[attr]);
    }
    input.addEventListener("input", function (e) {
      option.update.call(this, e);
    });

    const label = document.createElement("label");
    label.innerText = `${option.attrs.label}: `;
    label.appendChild(input);

    const options = document.querySelector(".options");
    options.appendChild(label);
  });
};
